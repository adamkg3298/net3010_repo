<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <a class="navbar-brand" href="#">Stonks for Gonks</a>
        <button class="navbar-toggler" type="button" data-toggle="tab" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
            aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <!-- added the class "nav" to the navbar. This class is important. Without it, the tab pane toggle will not work -->
            <ul class="nav navbar-nav mr-auto">
                <!-- I removed the "active" class from the nav-item -->
                <li class="nav-item">
                    <!-- Added the "data-toggle" attribute with the value "tab". Added the "href" attribute with the value "#tab1". This will toggle the tab pane with the ID of #tab1 -->
                    <!-- I added the "active" class to the nav-link -->
                    <a class="nav-link active" data-toggle="tab" href="#tab1">Home
                        <span class="sr-only">(current)</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#tab2">Weekly Picks</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link" data-toggle="tab" href="#tab3">Monthly Picks</a>
                </li>
              <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#tab4">FAQs</a>
                </li>
              <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#tab5">Experts</a>
                </li>
              <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#tab6">Contact Page</a>
                </li>
            </ul>
	
        </div>
    </nav>