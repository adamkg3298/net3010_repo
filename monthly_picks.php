<div id="tab3" class="tab-pane fade container">
	 <table style="table-layout: fixed">
		<tr>
			<th align="left">Ticker Symbol</th>
			<th align="left">Reason to buy</th> 
			<th align="left">Ideal purchase price</th>
			<th align="left">Target sell price</th> 
			<th align="left">Stop loss price</th>
			<th align="left">Reward to Risk Ratio</th>
		</tr>
		<tr>
			<td>x</td>
			<td style="word-wrap:break-word">Because x has proven good returns</td> 
			<td>120</td>
			<td>130</td>
			<td>115</td>
			<td>2:1</td>
		</tr>
		<tr>
			<td>y</td>
			<td>Because ys are the future</td> 
			<td>610</td>
			<td>650</td>
			<td>600</td>
			<td>4:1</td>
		</tr>
	</table>
</div>