<div id="tab2" class="tab-pane fade container">
	<style>
	table {
		font-family: arial, sans-serif;
		border-collapse: collapse;
		width: 100%;
	}
	td, th {
		border: 1px solid #dddddd;
		text-align: left;
		padding: 8px;
	}

	tr:nth-child(even) {
		background-color: #dddddd;
	}
	</style>
	<table style="table-layout: fixed">
		<tr>
			<th align="left">Ticker Symbol</th>
			<th align="left">Reason to buy</th> 
			<th align="left">Ideal purchase price</th>
			<th align="left">Target sell price</th> 
			<th align="left">Stop loss price</th>
			<th align="left">Reward to Risk Ratio</th>
		</tr>
		<tr>
			<td>AAPL</td>
			<td style="word-wrap:break-word">Because apple has proven good returns</td> 
			<td>120</td>
			<td>130</td>
			<td>115</td>
			<td>2:1</td>
		</tr>
		<tr>
			<td>TSLA</td>
			<td>Because EVs are the future</td> 
			<td>610</td>
			<td>650</td>
			<td>600</td>
			<td>4:1</td>
		</tr>
	</table>
</div>