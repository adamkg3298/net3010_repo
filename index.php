<!doctype html>
<html lang="en">

<head>
	<?php include ('head.php') ?>
</head>

<body>

	<?php include ('navbar.php') ?>
    <main class="container">
        <!-- Added a div to wrap around the tab content. Added the class "tab-content" to the div -->
        <div class="tab-content">
            <!-- Added a div to wrap around the content of the first tab pane. Added the classes "tab-pane", "fade", "show", "active" to the div. The first tab pane should have the "in" class. -->
            <!-- The "fade" class just adds a fade in effect and isn't required. -->
            <!-- If you add the "fade" class to the tab panes, you need to add the "show" class to the pane that you want to show when the page loads -->
            <?php include ('homepage.php') ?>
			<?php include ('weekly_picks.php') ?>
			<?php include ('monthly_picks.php') ?>
			<?php include ('faqs.php') ?>
			<?php include ('experts.php') ?>
			<?php include ('contact.php') ?>
    </main>
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ"
        crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm"
        crossorigin="anonymous"></script>
</body>

</html>
